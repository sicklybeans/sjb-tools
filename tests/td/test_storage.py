import pytest
import unittest.mock as mock
import sjb.common.config
from sjb.td.storage import Storage


def test_listname_none():
  s = Storage(listname=None)
  assert s.get_list_name() == 'todo'

def test_get_list_name_default():
  s = Storage()
  assert s.get_list_name() == 'todo'

@mock.patch('sjb.common.config.get_user_app_data_dir', return_value='blah')
def test_get_list_file_default(mock_get_data_dir):
  s = Storage()
  assert s.get_list_file() == 'blah/todo.json'
  mock_get_data_dir.assert_called_once_with('todo', suite_name='sjb')
