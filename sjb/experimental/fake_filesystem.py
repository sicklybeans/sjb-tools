"""
Module defines a mock filesystem suitable for running file-dependent tests.
"""

import enum
import os

class InternalException(Exception):
  pass

class FileType(enum.Enum):
  FILE = 0
  DIR = 1
  SYMLINK = 2

class File(object):

  def __init__(self, path, type, target=None):
    self.path = path
    self.type = type
    if target and type != FileType.SYMLINK:
      raise Exception('Can only specify links_to argument for symbolic links')

    self.target = target

class FileSystem(object):
  """A fake file system."""

  def __init__(self, working_dir='/'):
    self.file_map = {}

    if not working_dir.startswith('/'):
      raise Exception('Working directory must start with "/"')

    self._working_dir = os.path.normpath(working_dir)

    # Create root directory always
    self.file_map['/'] = File('/', FileType.DIR)
    if self._working_dir != '/':
      self.makedirs(self._working_dir)

  def abspath(self, path):
    if path.startswith('/'):
      return os.path.normpath(path)
    else:
      return os.path.normpath(os.path.join(self._working_dir, path))

  def chdir(self, path):
    if self.isdir(path):
      self._working_dir = self.abspath(path)
    elif self.exists(path):
      raise NotADirectoryError("Not a directory: '%s'" % path)
    else:
      raise FileNotFoundError("No such file or directory: '%s'" % path)

  def realpath(self, path):
    """
    Deferences symbolic link or returns path for regular file.
    """
    path = self.abspath(path)
    f = self.file_map.get(path, None)
    if f is None or f.type != FileType.SYMLINK:
      return path
    return self.realpath(f.target)

  def exists(self, path):
    """
    Returns true if file exists or if a symbolic link and the target exists.
    """
    path = self.realpath(path)
    f = self.file_map.get(path, None)

    return bool(f)

  def islink(self, path):
    """
    Returns true if a symbolic link exists with the given path.
    """
    path = self.abspath(path)
    f = self.file_map.get(path, None)

    return bool(f and f.type == FileType.SYMLINK)

  def isfile(self, path):
    """
    Returns true if a file or a symbolic link that points towards a file.
    """
    path = self.realpath(path)
    f = self.file_map.get(path, None)

    return bool(f and f.type == FileType.FILE)

  def isdir(self, path):
    """
    Returns true if a dir or a symbolic link that points towards a dir.
    """
    path = self.abspath(path)
    path = self.realpath(path)
    f = self.file_map.get(path, None)

    return bool(f and f.type == FileType.DIR)

  def mknod(self, path):
    self._ensure_not_exists(path)
    self._ensure_parent_exists(path, force=False)
    self._add_file(File(self.abspath(path), FileType.FILE))

  def mkdir(self, path):
    self._ensure_not_exists(path)
    self._ensure_parent_exists(path, force=False)
    self._add_file(File(self.abspath(path), FileType.DIR))

  def makedirs(self, path):
    self._ensure_not_exists(path)
    self._ensure_parent_exists(path, force=True)
    self._add_file(File(self.abspath(path), FileType.DIR))

  def symlink(self, target, path):
    self._ensure_not_exists(path)
    self._ensure_parent_exists(path, force=False)
    self._add_file(File(self.abspath(path), FileType.SYMLINK, target=self.abspath(target)))

  def remove(self, path):
    f = self._get_file(path)
    if f.type == FileType.DIR:
      raise IsADirectoryError("Is a directory: '%s'" % path)

    del self.file_map[self.abspath(path)]

  def rmdir(self, path):
    f = self._get_file(path)
    if f.type != FileType.DIR:
      raise NotADirectoryError("Not a directory: '%s'" % path)

    subfiles = self.listdir(path)
    if len(subfiles) != 0:
      raise OSError("Directory not empty: '%s'" % path)

    del self.file_map[self.abspath(path)]

  def listdir(self, path):
    actpath = self.realpath(path)
    f = self._get_file(actpath)
    if f.type != FileType.DIR:
      raise NotADirectoryError("Not a directory: '%s'" % path)

    files = []
    prefix = actpath + '/' if not actpath.endswith('/') else actpath
    num = prefix.count('/')
    for fname, f in self.file_map.items():
      if fname.startswith(prefix) and fname.count('/') == num and fname != prefix:
        files.append(fname[len(prefix):])
    return files

  def _get_file(self, path):
    newpath = self.abspath(path)
    f = self.file_map.get(newpath, None)
    if f == None:
      raise FileNotFoundError("No such file or directory: '%s'" % path)
    return f

  def _ensure_not_exists(self, path):
    newpath = self.abspath(path)
    if newpath in self.file_map:
      raise FileExistsError("File exists: '%s'" % path)

  def _add_file(self, fileobject):
    fileobject.path = self.abspath(fileobject.path)
    if fileobject.path in self.file_map:
      raise InternalException('SJB: Fix this')
    self.file_map[fileobject.path] = fileobject

  def _ensure_parent_exists(self, path, force=False):
    """
    Checks if parent dir exists and either creates it or raises an exception.
    """
    parent = os.path.dirname(self.abspath(path))

    if parent == '':
      return

    if not parent in self.file_map:
      if not force:
        raise FileNotFoundError("No such file or directory: '%s'" % path)
      else:
        self._ensure_parent_exists(parent, force=True)
        self._add_file(File(self.abspath(parent), FileType.DIR))

  # def add_dir(self, path, force=False):
  #   path = self.abspath()
  #   if path in self.file_map:
  #     raise FileExistsError("File exists: '%s'" % path)

  #   self.check_parent(path, force=force)

  #   self.file_map[path] = File(path, FileType.DIR)

  # def add_file(self, path, force=False):
  #   path = self.abspath()
  #   if path in self.file_map:
  #     raise FileExistsError("File exists: '%s'" % path)

  #   self.check_parent(path, force=force)

  #   self.file_map[path] = File(path, FileType.FILE)
