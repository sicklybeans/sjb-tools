import pytest
import json
import unittest.mock as mock
import sjb.common.config
from sjb.cs.storage import Storage


def test_listname_none():
  s = Storage(listname=None)
  assert s.get_list_name() == 'cheatsheet'

def test_get_list_name_default():
  s = Storage()
  assert s.get_list_name() == 'cheatsheet'

@mock.patch('sjb.common.config.get_user_app_data_dir', return_value='blah')
def test_get_list_file_default(mock_get_data_dir):
  s = Storage()
  assert s.get_list_file() == 'blah/cheatsheet.json'
  mock_get_data_dir.assert_called_once_with('cheatsheet', suite_name='sjb')
