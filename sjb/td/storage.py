"""
Module responsible for reading/writing TodoList objects to files.
"""

import sjb.common.storage
import sjb.td.classes

_APP = 'todo'
_DEFAULT_LIST_NAME = 'todo'


class Storage(sjb.common.storage.BaseStorage):

  def __init__(self, listname=None):
    super().__init__(
      sjb.td.classes.TodoListFactory(), _APP, listname or _DEFAULT_LIST_NAME)
