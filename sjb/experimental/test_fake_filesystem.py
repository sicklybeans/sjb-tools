import pytest
import os.path
import unittest.mock as mock

import tests.util.fake_filesystem
from tests.util.fake_filesystem import FileSystem

def test_test():
  fs = FileSystem()

def test_init_default_curdir():
  fs = FileSystem()
  assert fs.exists('/')
  assert fs.isdir('/')

def test_init_default_otherdir():
  fs = FileSystem(working_dir='/home/user')
  assert fs.isdir('/')
  assert fs.isdir('/home')
  assert fs.isdir('/home/user')

def test_init_bad_default_dir():
  with pytest.raises(Exception):
    fs = FileSystem(working_dir='home/user')

def test_abspath_normalize():
  fs = FileSystem()
  assert fs.abspath('some/././dir/../dir/file1.txt') == '/some/dir/file1.txt'
  assert fs.abspath('a/dir/') == '/a/dir'
  assert fs.abspath('/') == '/'
  assert fs.abspath('.') == '/'

def test_abspath_nondefault():
  fs = FileSystem(working_dir='/home/user')
  assert fs.abspath('a/dir/') == '/home/user/a/dir'
  assert fs.abspath('/other/dir') == '/other/dir'
  assert fs.abspath('/') == '/'
  assert fs.abspath('.') == '/home/user'
  assert fs.abspath('..') == '/home'
  assert fs.abspath('../../other') == '/other'

def test_chdir():
  fs = FileSystem(working_dir='/home/user')
  fs.mkdir('subdir')

  assert fs.abspath('.') == '/home/user'
  assert not fs.exists('/home/user/file1.txt')
  fs.mknod('file1.txt')
  assert fs.exists('/home/user/file1.txt')
  assert fs.exists('file1.txt')
  assert not fs.exists('/file1.txt')

  # now change the directory
  fs.chdir('subdir')
  assert fs.abspath('.') == '/home/user/subdir'
  assert fs.exists('/home/user/file1.txt')
  assert not fs.exists('file1.txt')
  assert not fs.exists('/file1.txt')
  assert fs.exists('../file1.txt')

def test_chdir_nonexists():
  fs = FileSystem(working_dir='/home/user')

  with pytest.raises(FileNotFoundError):
    fs.chdir('fakedir')

  fs.mknod('file1.txt')
  assert fs.exists('file1.txt')
  with pytest.raises(NotADirectoryError):
    fs.chdir('file1.txt')

def test_chdir_toself():
  fs = FileSystem(working_dir='/home/user')
  assert fs.realpath('.') == '/home/user'
  fs.chdir('.')
  assert fs.realpath('.') == '/home/user'

def test_chdir_goup():
  fs = FileSystem(working_dir='/home/user')
  assert fs.realpath('.') == '/home/user'
  fs.chdir('..')
  assert fs.realpath('.') == '/home'

def test_chdir_goup_at_root():
  fs = FileSystem(working_dir='/')
  assert fs.realpath('.') == '/'
  fs.chdir('..')
  assert fs.realpath('.') == '/'

def test_realpath_nondefault_dir():
  fs = FileSystem('/home/user')
  fs.mknod('file1.txt')
  fs.mkdir('sub')
  fs.mkdir('sub/dir')

  assert fs.realpath('file1.txt') == '/home/user/file1.txt'
  assert fs.realpath('sub/dir') == '/home/user/sub/dir'

def test_realpath_reg_files():
  fs = FileSystem()
  fs.mknod('file1.txt')
  fs.mkdir('sub')
  fs.mkdir('sub/dir')

  assert fs.realpath('file1.txt') == '/file1.txt'
  assert fs.realpath('sub/dir') == '/sub/dir'

def test_realpath_nonexist():
  fs = FileSystem()
  assert fs.realpath('file1.txt') == '/file1.txt'
  assert fs.realpath('sub/dir') == '/sub/dir'

def test_realpath_normalize_path():
  fs = FileSystem()
  fs.mknod('file1.txt')
  fs.mkdir('sub')
  assert fs.realpath('././file1.txt') == '/file1.txt'
  assert fs.realpath('sub/../sub/dir') == '/sub/dir'

def test_realpath_symlink_1layer():
  fs = FileSystem()
  fs.mknod('file1.txt')
  fs.symlink('file1.txt', 'symlink1.txt')

  assert fs.realpath('symlink1.txt') == '/file1.txt'

def test_realpath_symlink_3layers():
  fs = FileSystem()
  fs.mknod('file1.txt')
  fs.symlink('file1.txt', 'symlink1.txt')
  fs.symlink('symlink1.txt', 'symlink2.txt')
  fs.symlink('symlink2.txt', 'symlink3.txt')

  assert fs.realpath('symlink1.txt') == '/file1.txt'
  assert fs.realpath('symlink2.txt') == '/file1.txt'
  assert fs.realpath('symlink3.txt') == '/file1.txt'

def test_realpath_symlink_3layers_broken():
  fs = FileSystem()
  fs.symlink('file1.txt', 'symlink1.txt')
  fs.symlink('symlink1.txt', 'symlink2.txt')
  fs.symlink('symlink2.txt', 'symlink3.txt')

  assert fs.realpath('symlink1.txt') == '/file1.txt'
  assert fs.realpath('symlink2.txt') == '/file1.txt'
  assert fs.realpath('symlink3.txt') == '/file1.txt'

def test_realpath_root():
  fs = FileSystem()
  assert fs.realpath('.') == '/'

def test_exists_working_dir():
  fs = FileSystem(working_dir='/home/fakeuser')
  assert fs.exists('/')
  assert fs.exists('/home')
  assert fs.exists('/home/fakeuser')
  assert not fs.exists('/home/otheruser')

def test_exists_normalize():
  fs = FileSystem(working_dir='/home/fakeuser')

  assert fs.exists('.')
  assert fs.exists('..')
  assert fs.exists('../../../../..')
  assert fs.exists('../fakeuser/./././../../home')

def test_exists_complicated():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file1.txt')
  fs.mknod('/rootfile1.txt')
  fs.mknod('../file2.txt')

  assert fs.exists('file1.txt')
  assert fs.exists('/rootfile1.txt')
  assert fs.exists('../file2.txt')
  assert fs.exists('/home/file2.txt')
  assert fs.exists('/home/fakeuser/file1.txt')
  assert fs.exists('./file1.txt')

def test_exists_symlink_working():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file1.txt')
  fs.symlink('file1.txt', 'symlink1.txt')
  assert fs.exists('file1.txt')
  assert fs.exists('symlink1.txt')

def test_exists_symlink_broken():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.symlink('file1.txt', 'symlink1.txt')
  assert not fs.exists('symlink1.txt')

def test_exists_all_three_types():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('dir')
  fs.symlink('file', 'sfile')
  fs.symlink('dir', 'sdir')
  fs.symlink('broken', 'sbroken')
  assert fs.exists('dir')
  assert fs.exists('file')
  assert fs.exists('sdir')
  assert fs.exists('sfile')
  assert not fs.exists('broken')
  assert not fs.exists('sbroken')

def test_islink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('dir')
  fs.symlink('file.txt', 'sfile')
  fs.symlink('dir', 'sdir')
  fs.symlink('broken', 'sbroken')

  assert not fs.islink('file')
  assert not fs.islink('dir')
  assert not fs.islink('broken')
  assert fs.islink('sfile')
  assert fs.islink('sdir')
  assert fs.islink('sbroken')

  assert not fs.islink('nonexist')

def test_isfile():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('dir')
  fs.symlink('file', 'sfile')
  fs.symlink('dir', 'sdir')
  fs.symlink('broken', 'sbroken')

  assert fs.isfile('file')
  assert not fs.isfile('dir')
  assert not fs.isfile('broken')
  assert fs.isfile('sfile')
  assert not fs.isfile('sdir')
  assert not fs.isfile('sbroken')

  assert not fs.isfile('nonexist')

def test_isfile_normalize():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('subdir')
  fs.mknod('subdir/file2')

  assert fs.isfile('/home/fakeuser/file')
  assert fs.isfile('/home/./fakeuser/././file')
  assert fs.isfile('../fakeuser/subdir/../subdir/file2')
  assert fs.isfile('subdir/file2')

def test_isfile_multi_symlink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')

  fs.symlink('file', '/sfile1')
  fs.symlink('/sfile1', '../sfile2')
  assert fs.isfile('/home/sfile2')
  assert fs.isfile('/sfile1')
  assert fs.islink('/home/sfile2')
  assert fs.islink('/sfile1')

def test_isdir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('dir')
  fs.symlink('file.txt', 'sfile')
  fs.symlink('dir', 'sdir')
  fs.symlink('broken', 'sbroken')

  assert not fs.isdir('file')
  assert fs.isdir('dir')
  assert not fs.isdir('broken')
  assert not fs.isdir('sfile')
  assert fs.isdir('sdir')
  assert not fs.isdir('sbroken')

  assert not fs.isdir('nonexist')

def test_isdir_normalize():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('subdir')
  fs.mkdir('subdir/file2')

  assert not fs.isdir('/home/fakeuser/file')
  assert fs.isdir('/home/fakeuser')
  assert fs.isdir('..')
  assert fs.isdir('../..')
  assert fs.isdir('/home/fakeuser/')
  assert fs.isdir('/home/./fakeuser/././')
  assert fs.isdir('../fakeuser/subdir/../subdir/')
  assert fs.isdir('subdir')

def test_isdir_multi_symlink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')

  fs.symlink('subdir', '/sdir1')
  fs.symlink('/sdir1', '../sdir2')
  assert fs.isdir('/home/sdir2')
  assert fs.isdir('/sdir1')
  assert fs.islink('/home/sdir2')
  assert fs.islink('/sdir1')

def test_mknod():
  fs = FileSystem()
  fs.mknod('newfile.txt')
  assert fs.exists('newfile.txt')
  assert fs.isfile('newfile.txt')

def test_mknod_dupe():
  fs = FileSystem()
  fs.mknod('newfile.txt')
  assert fs.exists('newfile.txt')
  assert fs.isfile('newfile.txt')

  with pytest.raises(FileExistsError):
    fs.mknod('newfile.txt')

def test_mknod_bad_dir():
  fs = FileSystem()
  with pytest.raises(FileNotFoundError):
    fs.mknod('non/exist/newfile.txt')
  assert not fs.exists('non/exist/newfile.txt')

  fs.mknod('newfile.txt')
  assert fs.exists('newfile.txt')

def test_mknod_good_dir():
  fs = FileSystem()

  assert not fs.exists('/newfile.txt')
  fs.mknod('/newfile.txt')
  assert fs.exists('/newfile.txt')

  fs.mkdir('/dir')
  assert not fs.exists('/dir/newfile.txt')
  fs.mknod('/dir/newfile.txt')
  assert fs.exists('/dir/newfile.txt')

def test_mknod_normalize():
  fs = FileSystem()
  fs.mknod('././newfile.txt')
  assert fs.exists('newfile.txt')

def test_mknod_chdir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.makedirs('sub/dir/sub/dir')
  fs.chdir('./sub/dir/sub/dir')
  fs.mknod('newfile.txt')
  assert fs.isfile('newfile.txt')
  assert fs.isfile('/home/fakeuser/sub/dir/sub/dir/newfile.txt')

def test_mkdir():
  fs = FileSystem()
  fs.mkdir('subdir')
  assert fs.exists('subdir')
  assert fs.isdir('subdir')

def test_mkdir_dupe():
  fs = FileSystem()
  fs.mkdir('subdir')
  assert fs.exists('subdir')
  assert fs.isdir('subdir')

  with pytest.raises(FileExistsError):
    fs.mkdir('subdir')
  with pytest.raises(FileExistsError):
    fs.mknod('subdir')

def test_mkdir_bad_dir():
  fs = FileSystem()
  with pytest.raises(FileNotFoundError):
    fs.mkdir('non/exist')
  assert not fs.exists('non/exist')

def test_mkdir_normalize():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('../././newdir/../newdir')
  assert fs.exists('/home/newdir')
  assert fs.isdir('/home/newdir')

def test_makedirs():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('sub')
  fs.makedirs('sub/dir/sub/dir')
  assert fs.isdir('/home/fakeuser/sub')
  assert fs.isdir('/home/fakeuser/sub/dir')
  assert fs.isdir('/home/fakeuser/sub/dir/sub')
  assert fs.isdir('/home/fakeuser/sub/dir/sub/dir')

def test_makedirs_dupe():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.makedirs('sub/dir/sub/dir')
  with pytest.raises(FileExistsError):
    fs.makedirs('sub/dir/sub/dir')

def test_symlink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mkdir('dir')
  fs.symlink('file', 'sfile')
  fs.symlink('dir', 'sdir')
  fs.symlink('broken', 'sbroken')
  assert fs.islink('sdir')
  assert fs.islink('sfile')
  assert fs.islink('sbroken')
  assert fs.isdir('sdir')
  assert fs.isfile('sfile')
  assert not fs.exists('sbroken')
  assert fs.exists('sdir')
  assert fs.exists('sfile')

def test_symlink_dupe():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.mknod('sfile')

  with pytest.raises(FileExistsError):
    fs.symlink('file', 'sfile')

def test_symlink_dupe_link():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.symlink('file', 'sfile')
  with pytest.raises(FileExistsError):
    fs.symlink('file', 'sfile')

def test_symlink_bad_directory():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')

  with pytest.raises(FileNotFoundError):
    fs.symlink('file', 'sub/dir/sfile')

def test_symlink_good_directory():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.symlink('file', '/./home/fakeuser/../sfile')

  assert fs.islink('../sfile')
  assert fs.exists('../sfile')
  assert fs.isfile('../sfile')

def test_symlink_to_curdir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.symlink('.', 'thisdir')
  assert fs.islink('thisdir')
  assert fs.realpath('thisdir') == '/home/fakeuser'
  assert fs.isdir('thisdir')

def test_remove_file():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.remove('/home/fakeuser/file')
  assert not fs.exists('/home/fakeuser/file')
  assert not fs.isfile('/home/fakeuser/file')

def test_remove_file_nonexist():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.remove('file')
  with pytest.raises(FileNotFoundError):
    fs.remove('file')

def test_remove_symlink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  fs.symlink('file', 'sfile')

  assert fs.exists('sfile')
  assert fs.isfile('sfile')
  assert fs.islink('sfile')
  fs.remove('sfile')
  assert not fs.exists('sfile')
  assert not fs.isfile('sfile')
  assert not fs.islink('sfile')

def test_remove_symlink_broken():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.symlink('file', 'sfile')
  assert not fs.exists('sfile')
  assert not fs.isfile('sfile')
  assert fs.islink('sfile')
  fs.remove('sfile')
  assert not fs.exists('sfile')
  assert not fs.isfile('sfile')
  assert not fs.islink('sfile')

def test_remove_dir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')
  with pytest.raises(IsADirectoryError):
    fs.remove('subdir')

def test_rmdir_empty():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')

  assert fs.exists('subdir')
  assert fs.isdir('subdir')
  fs.rmdir('subdir')
  assert not fs.exists('subdir')
  assert not fs.isdir('subdir')

def test_rmdir_nonempty():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')
  fs.mknod('subdir/file1')
  fs.mknod('subdir/file2')

  with pytest.raises(OSError):
    fs.rmdir('subdir')

  fs.remove('subdir/file2')

  with pytest.raises(OSError):
    fs.rmdir('subdir')

  fs.remove('subdir/file1')

  assert fs.exists('subdir')
  assert fs.isdir('subdir')
  fs.rmdir('subdir')
  assert not fs.exists('subdir')
  assert not fs.isdir('subdir')

def test_rmdir_nonexist():
  fs = FileSystem(working_dir='/home/fakeuser')
  with pytest.raises(FileNotFoundError):
    fs.rmdir('subdir')

def test_rmdir_nondir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mknod('file')
  with pytest.raises(NotADirectoryError):
    fs.rmdir('file')

def test_rmdir_symlink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('dir')
  fs.symlink('dir', 'sdir')
  with pytest.raises(NotADirectoryError):
    fs.rmdir('sdir')
  fs.rmdir('dir')
  fs.remove('sdir')

def test_listdir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')
  fs.mknod('subdir/file1')
  fs.mknod('subdir/file2')
  fs.mkdir('subdir/subdir')
  fs.mknod('subdir/subdir/file3')

  result = fs.listdir('/home/fakeuser/subdir/')
  assert set(result) == {'file1', 'file2', 'subdir'}

def test_listdir_diff_dir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')
  fs.mknod('subdir/file1')
  fs.mknod('subdir/file2')
  fs.mkdir('subdir/subdir')
  fs.mknod('subdir/subdir/file3')
  fs.chdir('/')

  result = fs.listdir('/home/fakeuser/subdir/')
  assert set(result) == {'file1', 'file2', 'subdir'}

def test_listdir_rootdir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.chdir('/')
  fs.mknod('file')

  result = fs.listdir('/')
  assert set(result) == {'file', 'home'}

def test_listdir_normalize():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.chdir('/')
  fs.mknod('./././file')

  result = fs.listdir('.')
  assert set(result) == {'file', 'home'}

def test_listdir_nondir():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')
  fs.mknod('subdir/file1')
  with pytest.raises(NotADirectoryError):
    fs.listdir('subdir/file1')

def test_listdir_nonexist():
  fs = FileSystem(working_dir='/home/fakeuser')
  with pytest.raises(FileNotFoundError):
    fs.listdir('subdir/file1')

def test_listdir_symlink():
  fs = FileSystem(working_dir='/home/fakeuser')
  fs.mkdir('subdir')
  fs.mknod('subdir/file1')
  fs.mknod('subdir/file2')
  fs.mkdir('subdir/subdir')
  fs.mknod('subdir/subdir/file3')
  fs.symlink('subdir', 'sdir')

  result = fs.listdir('sdir')
  assert set(result) == {'file1', 'file2', 'subdir'}

def test_listdir_partial_match():
  pass
