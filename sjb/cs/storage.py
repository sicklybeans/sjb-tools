"""
Module responsible for reading/writing TodoList objects to files.
"""

import sjb.common.storage
import sjb.cs.classes

_APP = 'cheatsheet'
_DEFAULT_LIST_NAME = 'cheatsheet'


class Storage(sjb.common.storage.BaseStorage):

  def __init__(self, listname=None):
    super().__init__(
      sjb.cs.classes.CheatSheetFactory(), _APP, listname or _DEFAULT_LIST_NAME)
